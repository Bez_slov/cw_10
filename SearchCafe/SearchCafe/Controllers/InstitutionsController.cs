﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SearchCafe.Data;
using SearchCafe.Models;

namespace SearchCafe.Controllers
{
    public class InstitutionsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly UserManager<ApplicationUser> _userManager;


        public InstitutionsController(ApplicationDbContext context, IHostingEnvironment env, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            hostingEnvironment = env;
            _userManager = userManager;
        }

        // GET: Institutions
        public async Task<IActionResult> Index()
        {
            var list = await _context.Institutions.ToListAsync();
            foreach (var i in list)
            if (_context.Pictures.ToList() != null)
            {
                i.Images = _context.Pictures.Where(b => b.InstitutionId == i.Id).ToList();
            }
            return View(list);
        }

        // GET: Institutions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var institution = await _context.Institutions
                .SingleOrDefaultAsync(m => m.Id == id);
            
            if (institution == null)
            {
                return NotFound();
            }
            if (_context.Pictures.ToList() != null)
            {
                institution.Images = _context.Pictures.Where(i => i.InstitutionId == id).ToList();
            }
            if (_context.Feedbacks.ToList() != null)
            {
                institution.Feedbacks = _context.Feedbacks.Where(f => f.InstitutionId == id).OrderBy(f=>f.DateOfCreate).ToList();
            }

            IEnumerable<int> raiting = new List<int>() { 1, 2, 3, 4, 5 };
            ViewBag.Raiting = new SelectList(raiting);

            return View(institution);
        }

        // GET: Institutions/Create
        public IActionResult Create()
        {
            IEnumerable<int> raiting = new List<int>() { 1,2,3,4,5};
            ViewBag.Raiting = new SelectList(raiting);

            return View();
        }

        // POST: Institutions/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,UserId,Name,Discription,Raiting")] Institution institution,List<IFormFile> pictureFiles)
        {
            ApplicationUser us = await _userManager.GetUserAsync(this.User);

            if (ModelState.IsValid)
            {
                institution.UserId = us.Id;
                _context.Add(institution);  
                await _context.SaveChangesAsync();
                UploadFiles(institution, pictureFiles);
                return RedirectToAction(nameof(Index));
            }



            return View(institution);
        }

        // GET: Institutions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var institution = await _context.Institutions.SingleOrDefaultAsync(m => m.Id == id);
            if (institution == null)
            {
                return NotFound();
            }
            return View(institution);
        }

        // POST: Institutions/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,UserId,Name,Discription,Raiting")] Institution institution, List<IFormFile> pictureFiles)
        {
            if (id != institution.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(institution);
                    UploadFiles(institution, pictureFiles);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InstitutionExists(institution.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(institution);
        }

        // GET: Institutions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var institution = await _context.Institutions
                .SingleOrDefaultAsync(m => m.Id == id);
            if (institution == null)
            {
                return NotFound();
            }

            return View(institution);
        }

        // POST: Institutions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var institution = await _context.Institutions.SingleOrDefaultAsync(m => m.Id == id);
            _context.Institutions.Remove(institution);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool InstitutionExists(int id)
        {
            return _context.Institutions.Any(e => e.Id == id);
        }

        public void UploadFiles(Institution institution, List<IFormFile> pictureFiles)
        {
            if (pictureFiles != null)
            {
                foreach (var element in pictureFiles)
                {
                    var fileName = Path.Combine(hostingEnvironment.WebRootPath + "/images", Path.GetFileName(element.FileName));
                    element.CopyTo(new FileStream(fileName, FileMode.Create));
                    Picture picture = new Picture() { Puth = fileName, UserId = institution.UserId, InstitutionId = institution.Id, Name = element.FileName };
                    _context.Pictures.Add(picture);
                    _context.SaveChanges();
                }
            }
        }
    }
}
