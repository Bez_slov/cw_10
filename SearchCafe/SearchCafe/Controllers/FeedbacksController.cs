﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SearchCafe.Data;
using SearchCafe.Models;

namespace SearchCafe.Controllers
{
    public class FeedbacksController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment hostingEnvironment;


        public FeedbacksController(ApplicationDbContext context, IHostingEnvironment env)
        {
            _context = context;
            hostingEnvironment = env;
        }

        // GET: Feedbacks
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Feedbacks.Include(f => f.Institution);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Feedbacks/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var feedback = await _context.Feedbacks
                .Include(f => f.Institution)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (feedback == null)
            {
                return NotFound();
            }

            return View(feedback);
        }

        // GET: Feedbacks/Create
        public IActionResult Create()
        {
            ViewData["InstitutionId"] = new SelectList(_context.Institutions, "Id", "Id");
            return View();
        }

        // POST: Feedbacks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(FeedbackViewModel model, List<IFormFile> pictureFiles)
        {

            Feedback feedback = new Feedback()
            {
                InstitutionId = model.InstitutionId,
                UserId = model.UserId,
                Text = model.Text,
                DateOfCreate = DateTime.Now
            };
            _context.Add(feedback);
            await _context.SaveChangesAsync();

            //Institution institution = _context.Institutions.FirstOrDefault(i => i.Id == model.InstitutionId);
            _context.Update(model.Institution);
            await _context.SaveChangesAsync();


            if (pictureFiles != null)
            {
                UploadFiles(_context.Institutions.FirstOrDefault(f=>f.Id==model.InstitutionId), pictureFiles);
            }

            return RedirectToAction("Details", "Institutions", new { id = model.InstitutionId }); 
        }

        // GET: Feedbacks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var feedback = await _context.Feedbacks.SingleOrDefaultAsync(m => m.Id == id);
            if (feedback == null)
            {
                return NotFound();
            }
            ViewData["InstitutionId"] = new SelectList(_context.Institutions, "Id", "Id", feedback.InstitutionId);
            return View(feedback);
        }

        // POST: Feedbacks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,UserId,InstitutionId,Text")] Feedback feedback)
        {
            if (id != feedback.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(feedback);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FeedbackExists(feedback.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["InstitutionId"] = new SelectList(_context.Institutions, "Id", "Id", feedback.InstitutionId);
            return View(feedback);
        }

        // GET: Feedbacks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var feedback = await _context.Feedbacks
                .Include(f => f.Institution)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (feedback == null)
            {
                return NotFound();
            }

            return View(feedback);
        }

        // POST: Feedbacks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var feedback = await _context.Feedbacks.SingleOrDefaultAsync(m => m.Id == id);
            _context.Feedbacks.Remove(feedback);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FeedbackExists(int id)
        {
            return _context.Feedbacks.Any(e => e.Id == id);
        }

        public void UploadFiles(Institution institution, List<IFormFile> pictureFiles)
        {
            if (pictureFiles != null)
            {
                foreach (var element in pictureFiles)
                {
                    var fileName = Path.Combine(hostingEnvironment.WebRootPath + "/images", Path.GetFileName(element.FileName));
                    element.CopyTo(new FileStream(fileName, FileMode.Create));
                    Picture picture = new Picture() { Puth = fileName, UserId = institution.UserId, InstitutionId = institution.Id, Name = element.FileName };
                    _context.Pictures.Add(picture);
                    _context.SaveChanges();
                }
            }
        }
    }
}
