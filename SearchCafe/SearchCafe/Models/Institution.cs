﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SearchCafe.Models
{
    public class Institution
    {
        [Key]
        public int Id { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Discription { get; set; }

        public List<Feedback> Feedbacks { get; set; }
        public List<Picture> Images { get; set; }
        public List<Raiting> Raitings { get; set; }

        [Required]
        public double Raiting { get; set; }
    }
}
