﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SearchCafe.Models
{
    public class Feedback
    {
        [Key]
        public int Id { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        public int InstitutionId { get; set; }
        public Institution Institution { get; set; }

        public string Text { get; set; }

        public DateTime DateOfCreate { get; set; }
    }
}