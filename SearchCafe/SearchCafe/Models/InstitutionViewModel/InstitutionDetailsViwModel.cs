﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SearchCafe.Models.InstitutionViewModel
{
    public class InstitutionDetailsViwModel
    {
        public Institution Institution { get; set; }
        public Feedback Feedback { get; set; }
    }
}
