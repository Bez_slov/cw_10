﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SearchCafe.Models
{
    public class FeedbackViewModel
    {
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        public int InstitutionId { get; set; }
        public Institution Institution { get; set; }

        public string Text { get; set; }

        public DateTime DateOfCreate { get; set; }
    }
}
