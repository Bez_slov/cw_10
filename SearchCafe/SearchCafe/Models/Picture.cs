﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace SearchCafe.Models
{
    public class Picture
    {
        [Key]
        public int Id { get; set; }
        public string Puth { get; set;}
        public string Name { get; set; }


        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        public int InstitutionId { get; set; }
        public Institution Institution { get; set; }
    }
}